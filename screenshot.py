# Native imports
import time
import os
import sys
from datetime import datetime, timedelta

sleep_time = 0

def __run(url, sleep_time = None):
	''' Executar bot '''

	from bot import BetBot

	utc_date = datetime.utcnow().strftime('%Y-%m-%d-%H%M%S')

	# Iniciar instância do bot
	bot_instance = BetBot()

	print('Acessando: ' + url)

	# Acessar URL
	bot_instance.driver.get(url)

	sleep_time = sleep_time if bool(sleep_time) else bot_instance.wait_post_click

	print('Dormindo por ' + str(sleep_time) + 'seg...')

	time.sleep(bot_instance.wait_post_click)

	screenshots_dir = 'screenshots'

	if not os.path.exists(screenshots_dir) or not os.path.isdir(screenshots_dir):
		os.makedirs(screenshots_dir)

	screenshot_name = screenshots_dir + '/screenshot-' + str(utc_date) + '.png'

	# Salvar screenshot
	bot_instance.driver.save_screenshot(screenshot_name)

	# Encerrar processo
	bot_instance.driver.quit()

args = sys.argv

if len(args) > 1:

	url = args[1]

	sleep_time = None

	if args[1] == 'bet365':
		url = 'https://www.bet365.com/#/AVR/B24/R^1/'

	elif args[1] == 'proxy':
		url = 'https://ifconfig.me'

	elif args[1] == 'nowsecure':
		sleep_time = 10
		url = 'https://nowsecure.nl'

	__run(url, sleep_time)