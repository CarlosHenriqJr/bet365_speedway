# Native imports
import traceback
import json
from time import sleep

# Vendor imports
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import undetected_chromedriver as uc
from fp.fp import FreeProxy
from fake_useragent import UserAgent

# My imports
from utils.env_utils import env
from utils.log_utils import Log

Log.init()

class BetBot:

	def __init__(self):

		self.driver = self.__get_driver()
		
		self.wait_post_click = env('wait_post_click')

	def __get_random_proxy(self, country_id = None, rand = True, anonym = True):
		''' Obter um proxy '''

		self.__log('debug', '[GET-DRIVER]', 'Obtendo proxy aleatório...')

		try:

			ua = UserAgent()

			user_agent = ua.random

			fp = FreeProxy(
				country_id = country_id,
				rand = rand,
				anonym = anonym
			)

			proxy = fp.get()

			ip = proxy.split("://")[1]

			if user_agent and ip:
				
				return {
					'status': True,
					'user_agent': user_agent,
					'ip': ip
				}

			else:
				raise Exception('Não foi possível obter um UserAgent ou Proxy')

		except Exception as e:

			return {
				'status': False,
				'message': str(e)
			}

	def __get_driver(self):
		''' Obter driver e configurações '''

		options = uc.ChromeOptions()

		user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36'

		''' proxy_response = self.__get_random_proxy()

		if proxy_response['status'] == True:

			user_agent = proxy_response['user_agent']
			self.__log('debug', '[GET-DRIVER]', 'UserAgent obtido: ' + user_agent)

			proxy_ip = proxy_response['ip']
			self.__log('debug', '[GET-DRIVER]', 'Proxy obtido: ' + proxy_ip)

			options.add_argument(f'--proxy-server={proxy_ip}')

		else:
			self.__log('debug', '[GET-DRIVER]', 'Não foi possível obter um proxy aleatório: ' + proxy_response['message']) '''

		options.add_argument(f'user-agent={user_agent}')

		# another way to set profile is the below (which takes precedence if both variants are used
		options.add_argument('--user-data-dir=/tmp/profile')

		# just some options passing in to skip annoying popups
		options.add_argument('--no-first-run --no-service-autorun --password-store=basic')

		# window size
		options.add_argument("window-size=1280,720")

		# headless
		#options.add_argument('--headless')

		driver = uc.Chrome(
			options = options,
		)

		return driver

	def __find_element(self, selector, parent = None, many = False, manyIndex = None):
		''' Encontrar elemento dentro da tela '''

		try:

			if parent == None:
				parent = self.driver

			if many:

				result = parent.find_elements(By.CSS_SELECTOR, selector)

				if manyIndex != None:

					if len(result) > manyIndex:
						result = result[manyIndex]
					
					else:
						self.__log('error', '[FIND-ELEMENT]', 'Não foi possível identificar o filho #' + str(manyIndex) + ' da busca "' + selector + '" (' + str(len(result)) + ')')
						result = None

			else:
				result = parent.find_element(By.CSS_SELECTOR, selector)

			return result

		except Exception as e:
			return None

	def __log(self, method, prefix, message):
		''' Logar mensagem '''

		Log.dynamic(method, prefix + ' ' + message)

	def get_results(self):
		''' Obter resultados de cada um dos campeonatos '''

		log_prefix = '[RESULTS][GET]'

		results = []

		self.__log('debug', log_prefix, 'Buscando aba de resultados...')

		results_tab = self.__find_element('.vr-ResultsNavBarButton')

		if results_tab:

			self.__log('debug', log_prefix, 'Clicando na aba de resultados...')

			results_tab.click()
			sleep(self.wait_post_click)

			meeting_results = self.get_meeting_results()

			# Concatenar resultados
			results = results + meeting_results

		else:
			raise Exception('Não foi possível encontrar a aba de resultados dentro da tela')

		results.reverse()

		return results

	def get_meeting_results(self):
		''' Obter resultados do campeonato atualmente selecionado '''

		log_prefix = '[RESULTS][GET]'

		results = []

		self.__log('debug', log_prefix, 'Buscando elementos que representam os últimos resultados...')

		results_elements = self.__find_element(
			selector = '.vrr-MarketGroupOutrightRaceDescription',
			many = True
		)

		if results_elements and len(results_elements) > 0:

			for idx, result_element in enumerate(results_elements):

				self.__log('error', log_prefix, 'Identificando valores do resultado #' + str(idx+1) + '...')

				try:

					self.__log('debug', log_prefix, 'Identificando horário do evento do resultado #' + str(idx+1) + '...')

					race_time = self.__get_race_time(result_element)

					self.__log('debug', log_prefix, 'Horário do evento identificado #' + str(idx+1) + ': ' + race_time)

					self.__log('debug', log_prefix, 'Identificando pódio do resultado #' + str(idx+1) + '...')

					race_podium = self.__get_race_podium(result_element)

					self.__log('debug', log_prefix, 'Pódio identificado #' + str(idx+1) + ': ' + json.dumps(race_podium))

					result = {
						'time': race_time,
						'podium': race_podium
					}

					results.append(result)

				except Exception as e:
					self.__log('error', log_prefix, 'Não foi possível identificar os valores do resultado #' + str(idx+1) + ': ' + str(e))

		else:
			raise Exception('Não foi possível encontrar os elementos que representam os últimos resultados dentro da tela')

		return results

	def get_odds(self):
		''' Obter odds das corridas futuras '''

		log_prefix = '[ODDS][GET]'

		results = []

		for i in range(6):

			try:

				self.__log('debug', log_prefix, 'Buscando odds na aba #' + str(i+1) + '...')

				tab_button = self.__find_element(
					selector = '.vr-EventTimesNavBarButton',
					many = True,
					manyIndex = i
				)

				if tab_button and tab_button.text:

					race_time = tab_button.text

					self.__log('debug', log_prefix, 'Clicando na aba #' + str(i+1) + ': ' + race_time)

					tab_button.click()
					sleep(self.wait_post_click)

					self.__log('debug', log_prefix, 'Buscando elementos que armazenam as odds na aba #' + str(i+1) + '...')

					odd_text_elements = self.__find_element(
						selector = '.vr-ParticipantVirtualOddsOnly_Odds',
						many = True
					)

					if odd_text_elements and len(odd_text_elements) > 0:

						race_odds = []

						for odd_text_element in odd_text_elements:

							odd = odd_text_element.text

							if odd:
								race_odds.append(odd_text_element.text)

						if len(race_odds) == 4:

							self.__log('debug', log_prefix, 'Odds identificadas na aba #' + str(i+1) + ': ' + json.dumps(race_odds))

							result = {
								'time': race_time,
								'odds': race_odds
							}

							results.append(result)

						else:
							raise Exception('Não foi possível identificar corretamente as 4 odds presentes na aba')

					else:
						raise Exception('Não foi possível encontrar os elementos que armazenam as odds')

				else:
					raise Exception('Não foi possível identificar uma aba válida')

			except Exception as e:
				self.__log('error', log_prefix, 'Não foi foi possível obter as odds na aba #' + str(i+1) + ': ' + str(e))

		return results

	def __get_race_time(self, result_element):
		''' Obter tempo da corrida '''

		try:

			race_name_element = self.__find_element(
				selector = '.vrr-MarketGroupOutrightRaceDescription_RaceName',
				parent = result_element
			)

			if race_name_element:
				
				# Extrair texto do elemento
				race_name_text = race_name_element.text

				# Separar horário do nome da pista
				race_name_parts = race_name_text.split(' ')
				
				# Extrair apenas horário
				race_time_tmp = race_name_parts[0]

				# Converter 3.16 -> 03:16
				race_time_tmp_parts = race_time_tmp.split('.')
				race_time_tmp_parts[0] = race_time_tmp_parts[0].rjust(2, '0')
				
				# Formatar valor
				race_time = ':'.join(race_time_tmp_parts)

				return race_time

			else:
				raise Exception('Não foi possível encontrar o elemento que armazena o horário do evento')

		except Exception as e:
			raise Exception('Falha ao obter horário do evento: ' + str(e))

	def __get_race_podium(self, result_element):
		''' Obter pódio da corrida '''

		try:

			podium_text_element = self.__find_element(
				selector = '.vrr-ResultParticipant_Text',
				parent = result_element,
				many = True,
				manyIndex = 1
			)

			if podium_text_element:

				podium_text = podium_text_element.text
				podium = podium_text.split('-')

				for i in range(4):

					j = str(i+1)

					if j not in podium:
						podium.append(j)

				return podium

			else:
				raise Exception('Não foi possível encontrar o elemento que armazena o pódio do evento')

		except Exception as e:
			raise Exception('Falha ao obter pódio do evento: ' + str(e))
