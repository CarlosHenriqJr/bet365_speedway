# Native imports
import datetime as dt

# Vendor imports
import psycopg2
from psycopg2.extras import DictCursor, NamedTupleCursor
from psycopg2.pool import SimpleConnectionPool

# My imports
from __main__ import db_pool

def get_conn():
    ''' Obter uma conexão do pool de conexões '''
    
    return db_pool.getconn()
    
def conn_close(conn):
    ''' Encerrar conexão '''
    
    if conn:
        conn.cursor().close()
        db_pool.putconn(conn)
    
def execute_query(query, fetch_all=False, fetch_one=False):
    ''' Executar consulta query '''
    
    conn = None
    
    try:
    
        conn = get_conn()
        cursor = conn.cursor(cursor_factory=DictCursor)
        cursor.execute(query)
    
        if fetch_one:
            result = cursor.fetchone()
            
        else:
            result = cursor.fetchall()
            
        return result    
        
    except Exception as e:
        raise e;
        
    finally:
        conn_close(conn)
    
def conn_commit(conn):
    ''' Realizar commit na conexão '''

    if (conn):
        conn.commit()

def conn_rollback(conn):
    ''' Realizar rollback na conexão '''
    
    if (conn):
        conn.rollback()

def insert_query(table, data):
    ''' Executar query de inserção (conexão e commit automático) '''
    
    conn = None
    
    try:
        
        conn = get_conn()

        result = insert_query_conn(conn, table, data)

        return result
        
    except Exception as e:
        conn_rollback(conn)
        raise e;
    
    finally:
        conn_commit(conn)
        conn_close(conn)
    
def insert_query_conn(conn, table, data):
    ''' Executar query de inserção '''
    
    try:
        
        keys = data.keys()
        columns_str = ', '.join(keys)
        
        values_arr = ['%({})s'.format(k) for k in keys]
        values_str = ', '.join(values_arr)

        query = 'insert into {0} ({1}) values ({2}) returning id'.format(table, columns_str, values_str)
        
        cursor = conn.cursor()
        cursor.execute(query, data)
        
        inserted_id = cursor.fetchone()[0]
        return inserted_id
        
    except Exception as e:
        raise e;
    
def update_query(table, data, id):
    ''' Executar query de update (conexão e commit automático) '''
    
    conn = None
    
    try:
        
        conn = get_conn()
        
        result = update_query_conn(conn, table, data, id)

        return result
        
    except Exception as e:
        conn_rollback(conn)
        raise e;
    
    finally:
        conn_commit(conn)
        conn_close(conn)
    
def update_query_conn(conn, table, data, id):
    ''' Executar query de update '''
    
    try:
        
        keys = data.keys()
        
        sets_arr = [(k + '=' + '%({})s'.format(k)) for k in keys]
        sets_str = ', '.join(sets_arr)
        
        query = 'update {0} set {1} where id = {2}'.format(table, sets_str, id)
        
        cursor = conn.cursor()
        cursor.execute(query, data)
        
        return {"status": True}
        
    except Exception as e:
        raise e
    
def delete_query(table, id=None, where=None):
    ''' Executar query de remoção (conexão e commit automático) '''
    
    conn = None
    
    try:

        conn = get_conn()

        result = delete_query_conn(conn, table, id, where)

        return result
        
    except Exception as e:
        conn_rollback(conn)
        raise e;
    
    finally:
        conn_commit(conn)
        conn_close(conn)
    
def delete_query_conn(conn, table, id=None, where=None):
    ''' Executar query de update '''
    
    try:
        
        where_arr = []
        
        if (where == None and id != None):
            where = { "id": id }
        
        for key in where:
            where_arr.append(key + ' = ' + where[key])
            
        where_str = ' and '.join(where_arr)
        query = 'delete from ' + table + ' where ' + where_str
        
        cursor = conn.cursor()
        cursor.execute(query)
        
        return {
            "status": True
        }
        
    except Exception as e:

        err = str(e)

        if ("foreign key constraint" in err):
            return { "status": False, "error": "Não é possível remover esse registro porque ele está atrelado a algum outro no banco de dados" }
        
        else:
            raise e;

def array_column(arr, type):
    ''' Retornar a string que será salva no banco de dados no formato array '''
    
    arr = list(map(lambda x: str(x), arr))

    if type == "string":    
        arr = list(map(lambda x: "'" + str(x) + "'", arr))
        
    column = "{" + (", ".join(arr)) + "}"
    return column