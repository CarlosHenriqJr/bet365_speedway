# global imports
import logging
import datetime as dt
import os
from logging.handlers import TimedRotatingFileHandler

# module imports
from utils.env_utils import env

class Log():
  
    logger = None

    levels = {
        "CRITICAL": logging.CRITICAL,
        "ERROR": logging.ERROR,
        "WARNING": logging.WARNING,
        "INFO": logging.INFO,
        "DEBUG": logging.DEBUG,
    }

    @staticmethod
    def init():
        """ Inicializa o log da aplicação """

        if Log.logger:
            return

        debug = env("debug")

        log_path = env('project_path') + '/logs'
        
        if not os.path.exists(log_path) or not os.path.isdir(log_path):

            if debug:
                os.makedirs(log_path)
                
            else:
                raise Exception("O diretório '" + log_path + "' para logs não existe!")
        
        log_path = log_path + "/scrap.log"

        # Create a file handler
        log = logging.getLogger(__name__)

        handler = TimedRotatingFileHandler(log_path, when = "midnight", interval = 1, backupCount = 15)
        handler.suffix = "%Y%m%d"
        
        # Create a logging format
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)

        # Add the file handler to the logger
        log.addHandler(handler)
        
        # Salvar váriavel estática
        Log.logger = log

        # Definir level
        Log.setLevel(env('log_level'))

    @staticmethod
    def setLevel(level):
        ''' Alterar level do log '''

        levels = Log.levels

        if level in levels:
            Log.logger.setLevel(levels[level])

    @staticmethod
    def debug(message: str):
        Log.dynamic("debug", message)

    @staticmethod
    def info(message: str):
        Log.dynamic("info", message)

    @staticmethod
    def warning(message: str):
        Log.dynamic("warning", message)

    @staticmethod
    def error(message: str):
        Log.dynamic("error", message)
        
    @staticmethod
    def critical(message: str):
        Log.dynamic("critical", message)

    @staticmethod
    def dynamic(key: str, message: str):
        ''' Chamar dinamicamente alguma função do log '''

        if env("debug") == True:
            date = dt.datetime.now()
            dateStr = date.strftime("%Y-%m-%d %H:%M:%S")
            print(dateStr + ' - ' + key.upper() + ' - ' + (" " if (message[0] != '[') else "") + message)
        
        if (key == "debug"):
            Log.logger.debug(message)

        elif (key == "info"):
            Log.logger.info(message)
        
        elif (key == "warning"):
            Log.logger.warning(message)

        elif (key == "error"):
            Log.logger.error(message)

        elif (key == "critical"):
            Log.logger.critical(message)